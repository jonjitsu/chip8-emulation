# Package

version       = "0.1.0"
author        = "jonjitsu"
description   = "A chip8 emulator in Nim"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["chip8nim"]


# Dependencies

requires "nim >= 1.4.0"

task tdocs, "Test docs":
    exec "nim doc --verbosity:0  src/sequtils.nim"