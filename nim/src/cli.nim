import machine
import rom
import parseopt
import tables
import os
import strutils
import strformat
import dasm


const Usage = fmt"""
$1 COMMAND [OPTIONS] [ARG1, ...]

Command:
    run FILEPATH ...         Run the given roms
    asm FILEPATH ...         Assemble source files into binary roms
    dasm FILEPATH ...        Disassemble roms into assembler
"""

proc printUsage(exitCode: int = 0) =
    echo (Usage % [
        lastPathPart(getAppFilename()),
    ])
    quit(exitCode)

proc printError(msg: string) =
    stderr.writeLine("ERROR: " & msg)

type
    Opts = Table[string, string]
    CommandLine = ref object
        cmd: string
        args: seq[string]
        options: Opts

proc parseCli(cmdline: seq[TaintedString]): CommandLine =
    new(result)
    var 
        parser = initOptParser(cmdline)

    for kind, key, val in parser.getopt():
        case kind
        of cmdArgument:
            if result.cmd == "":
                if key == "help":
                    printUsage()
                else: result.cmd = key
            else: result.args.add(key)
        of cmdLongOption, cmdShortOption:
            if key == "h" or key == "help":
                printUsage()
            result.options[key] = val
        of cmdEnd: raise newException(Defect, "parseopt bug detected")

import displays/noop as display
import controls/noop as control

proc run(opts: Opts, filepath: seq[string]) =
    var 
        display: NoopDisplay 
        control: NoopControl
        emulator = newMachine(display, control)
    for filep in filepath:
        let rom = loadRomFromFile(filep)
        emulator.runRom(rom)

proc disassemble(opts: Opts, filepath: seq[string]) =
    for filep in filepath:
        let 
            header = "; Generated from $1\n" % [filep]
            romData = toUint16(loadRomFromFile(filep))
            rom = disassembleRomData(romData)
        echo header & rom.render()

proc assemble(opts: Opts, args: seq[string]) =
    raise newException(Exception, "@TODO")


# proc routeCli(cmdline: CommandLine) =
#     var fn = case cmdline.cmd
#     of "run": run
#     of "disassemble": disassemble
#     of "assemble": assemble
#     else: nil
#     if fn.isNil:
#         printError("Unkown command " & cmdline.cmd)
#         printUsage(1)
#     else:
#         fn(cmdline.options, cmdline.args)

proc routeCli(cmdline: CommandLine) =
    let route {.global.} = {
        "run": run,
        "dasm": disassemble,
        "asm": assemble,
    }.toTable

    if route.hasKey(cmdline.cmd):
        try:
            route[cmdline.cmd](cmdline.options, cmdline.args)
        except Exception as e:
            printError(e.msg)
    else:
        printError("Unkown command " & cmdline.cmd)
        printUsage(1)

when isMainModule:
    var cli = parseCli(os.commandLineParams())
    routeCli(cli)
