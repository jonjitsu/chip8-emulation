import abstract

type
    NoopControl* = ref object of AControl

method isPressed*(c: AControl, k: uint8): bool = false
method getKey*(c: AControl): uint8 = 0