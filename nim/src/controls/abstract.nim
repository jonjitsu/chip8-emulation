type
    AControl* = ref object of RootObj

method isPressed*(c: AControl, k: uint8): bool {.base.} = discard
method getKey*(c: AControl): uint8 {.base.} = discard