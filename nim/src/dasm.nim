import strutils
import tables
from machine import EntryPoint
from columnize import render
import sequtils
from strutils import join


type
    Instruction = tuple
        opcode: string
        operands: seq[string]
        raw: uint16
    Labels = Table[uint16, string]
    Rom = ref object
        labels: Labels
        instructions: seq[Instruction]

const UndefinedInstruction: Instruction = ("UNDEFINED", @[], 0x0000'u16)


proc address(ins: uint16): string {.inline.} =
    "#" & (ins and 0x0FFF).toHex(3)

proc label(ins: uint16): string {.inline.} =
    "L" & (ins and 0x0FFF).toHex(3)

proc vx(ins: uint16): string {.inline.} =
    "V" & ((ins and 0x0F00) shr 8).toHex(1)

proc vy(ins: uint16): string {.inline.} =
    "V" & ((ins and 0x00F0) shr 4).toHex(1)

proc byte(ins: uint16): string {.inline.} =
    "#" & (ins and 0x00FF).toHex(2)

proc nibble(ins: uint16): string {.inline.} =
    "#" & (ins and 0x000F).toHex(1)

proc disassembleInstruction(ins: uint16): Instruction =
    case (ins and 0XF000)
    of 0x0000: 
        case (ins and 0x0FFF)
        of 0x00E0: ("CLS", @[], ins)
        of 0x00EE: ("RET", @[], ins)
        else: ("SYS", @[address(ins)], ins)
    of 0x1000: ("JP", @[label(ins)], ins)
    of 0x2000: ("CALL", @[label(ins)], ins)
    of 0x3000: ("SE", @[vx(ins), byte(ins)], ins)
    of 0x4000: ("SNE", @[vx(ins), byte(ins)], ins)
    of 0x5000: ("SE", @[vx(ins), vy(ins)], ins)
    of 0x6000: ("LD", @[vx(ins), byte(ins)], ins)
    of 0x7000: ("ADD", @[vx(ins), byte(ins)], ins)
    of 0x8000: 
        case (ins and 0x000F)
        of 0x0000: ("LD", @[vx(ins), vy(ins)], ins)
        of 0x0001: ("OR", @[vx(ins), vy(ins)], ins)
        of 0x0002: ("AND", @[vx(ins), vy(ins)], ins)
        of 0x0003: ("XOR", @[vx(ins), vy(ins)], ins)
        of 0x0004: ("ADD", @[vx(ins), vy(ins)], ins)
        of 0x0005: ("SUB", @[vx(ins), vy(ins)], ins)
        of 0x0006: ("SHR", @[vx(ins), vy(ins)], ins)
        of 0x0007: ("SUBN", @[vx(ins), vy(ins)], ins)
        of 0x000E: ("SHL", @[vx(ins), vy(ins)], ins)
        else: UndefinedInstruction 
    of 0x9000: ("SNE", @[vx(ins), vy(ins)], ins)
    of 0xA000: ("LD", @["I", address(ins)], ins)
    of 0xB000: ("JP", @["V0", address(ins)], ins)
    of 0xC000: ("RND", @[vx(ins), byte(ins)], ins)
    of 0xD000: ("DRW", @[vx(ins), vy(ins), nibble(ins)], ins)
    of 0xE000: 
        case (ins and 0x00FF)
        of 0x009E: ("SKP", @[vx(ins)], ins)
        of 0x00A1: ("SKNP", @[vx(ins)], ins)
        else: UndefinedInstruction
    of 0xF000: 
        case (ins and 0x00FF)
        of 0x0007: ("LD", @[vx(ins), "DT"], ins)
        of 0x000A: ("LD", @[vx(ins), "K"], ins)
        of 0x0015: ("LD", @["DT", vx(ins)], ins)
        of 0x0018: ("LD", @["ST", vx(ins)], ins)
        of 0x001E: ("ADD", @["I", vx(ins)], ins)
        of 0x0029: ("LD", @["F", vx(ins)], ins)
        of 0x0033: ("LD", @["B", vx(ins)], ins)
        of 0x0055: ("LD", @["[I]", vx(ins)], ins)
        of 0x0065: ("LD", @[vx(ins), "[I]"], ins)
        else: UndefinedInstruction 
    else: UndefinedInstruction 
    # echo (ins.toHex, (ins and 0XF000).toHex, result)

proc disassembleRomData*(rom: seq[uint16]): Rom =
    new(result)
    for ins in rom:
        let instruction = disassembleInstruction(ins)
        let msb = instruction.raw and 0xF000
        if msb == 0x1000 or msb == 0x2000:
            let address = instruction.raw and 0x0FFF
            result.labels[address] = label(instruction.raw)
        result.instructions.add(instruction)

proc renderLabelForAddress(rom: Rom, address: uint16): string =
    if rom.labels.hasKey(address):
        result = rom.labels[address] & ":"

proc render*(rom: Rom): string =
    var lineCount = 0
    proc columnize(i: Instruction): seq[string] =
        let 
            address = cast[uint16](lineCount * 2 + EntryPoint)
            label = rom.renderLabelForAddress(address)
            operands = i.operands.join(", ")
            comment = "; " & i.raw.toHex(4)
        lineCount += 1
        @[label, i.opcode, operands, comment]
    rom.instructions.map(columnize).render()


template checkDasm(ins: uint16, expected: Instruction) =
    block:
        let actual = disassembleInstruction(ins)
        doAssert actual == expected, "call(" & ins.toHex() & "): " & $actual

when isMainModule:
    doAssert byte(0xFFFF) == "#FF"
    doAssert byte(0xF0FF) == "#FF"
    doAssert byte(0xF00F) == "#0F"
    doAssert byte(0xF0F0) == "#F0"
    for x in 0 .. 15:
        let 
            offset = cast[uint16](x shl 4)
            ins: uint16 = (0x0000 or offset)
            expected = "V" & x.toHex(1)
        doAssert vy(ins) == expected, vy(ins) & " == " & expected
    for x in 0 .. 15:
        let 
            offset = cast[uint16](x shl 8)
            ins: uint16 = (0x0000 or offset)
            expected = "V" & x.toHex(1)
        doAssert vx(ins) == expected, vx(ins) & " == " & expected
    doAssert label(0x0222) == "L222"
    checkDasm(0x00E0, ("CLS", @[], 0x00E0'u16))
    checkDasm(0x00EE, ("RET", @[], 0x00EE'u16))
    checkDasm(0x01EE, ("SYS", @["#1EE"], 0x01EE'u16))
    checkDasm(0x1200, ("JP", @["L200"], 0x1200'u16))
    checkDasm(0x2230, ("CALL", @["L230"], 0x2230'u16))
    checkDasm(0x3230, ("SE", @["V2", "#30"], 0x3230'u16))
    checkDasm(0x4330, ("SNE", @["V3", "#30"], 0x4330'u16))
    checkDasm(0x5340, ("SE", @["V3", "V4"], 0x5340'u16))
    checkDasm(0x6340, ("LD", @["V3", "#40"], 0x6340'u16))
    checkDasm(0x7340, ("ADD", @["V3", "#40"], 0x7340'u16))
    checkDasm(0x8340, ("LD", @["V3", "V4"], 0x8340'u16))
    checkDasm(0x8341, ("OR", @["V3", "V4"], 0x8341'u16))
    checkDasm(0x8342, ("AND", @["V3", "V4"], 0x8342'u16))
    checkDasm(0x8343, ("XOR", @["V3", "V4"], 0x8343'u16))
    checkDasm(0x8344, ("ADD", @["V3", "V4"], 0x8344'u16))
    checkDasm(0x8345, ("SUB", @["V3", "V4"], 0x8345'u16))
    checkDasm(0x8346, ("SHR", @["V3", "V4"], 0x8346'u16))
    checkDasm(0x8347, ("SUBN", @["V3", "V4"], 0x8347'u16))
    checkDasm(0x834E, ("SHL", @["V3", "V4"], 0x834E'u16))
    checkDasm(0x9340, ("SNE", @["V3", "V4"], 0x9340'u16))
    checkDasm(0xA340, ("LD", @["I", "#340"], 0xA340'u16))
    checkDasm(0xB340, ("JP", @["V0", "#340"], 0xB340'u16))
    checkDasm(0xC340, ("RND", @["V3", "#40"], 0xC340'u16))
    checkDasm(0xD34D, ("DRW", @["V3", "V4", "#D"], 0xD34D'u16))
    checkDasm(0xE39E, ("SKP", @["V3"], 0xE39E'u16))
    checkDasm(0xE3A1, ("SKNP", @["V3"], 0xE3A1'u16))
    checkDasm(0xF307, ("LD", @["V3", "DT"], 0xF307'u16))
    checkDasm(0xF30A, ("LD", @["V3", "K"], 0xF30A'u16))
    checkDasm(0xF315, ("LD", @["DT", "V3"], 0xF315'u16))
    checkDasm(0xF318, ("LD", @["ST", "V3"], 0xF318'u16))
    checkDasm(0xF31E, ("ADD", @["I", "V3"], 0xF31E'u16))
    checkDasm(0xF329, ("LD", @["F", "V3"], 0xF329'u16))
    checkDasm(0xF333, ("LD", @["B", "V3"], 0xF333'u16))
    checkDasm(0xF355, ("LD", @["[I]", "V3"], 0xF355'u16))
    checkDasm(0xF365, ("LD", @["V3", "[I]"], 0xF365'u16))
