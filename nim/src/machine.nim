import strutils
import algorithm
import random
import math

proc toBcd(b: uint8): (uint8, uint8, uint8) =
    var
        bb = cast[int](b)
        h = cast[uint8](floor(bb / 100))
        t = cast[uint8](floor(bb mod 100 / 10))
        d = cast[uint8](bb mod 100 mod 10)
    return (h, t, d)

const 
    RamSize = 4096
    EntryPoint* = 0x200
    FontSize = 5
    Fonts: array[FontSize*16, uint8] = [
        0xF0'u8, 0x90'u8, 0x90'u8, 0x90'u8, 0xF0'u8, # 0
        0x20'u8, 0x60'u8, 0x20'u8, 0x20'u8, 0x70'u8, # 1
        0xF0'u8, 0x10'u8, 0xF0'u8, 0x80'u8, 0xF0'u8, # 2
        0xF0'u8, 0x10'u8, 0xF0'u8, 0x10'u8, 0xF0'u8, # 3
        0x90'u8, 0x90'u8, 0xF0'u8, 0x10'u8, 0x10'u8, # 4
        0xF0'u8, 0x80'u8, 0xF0'u8, 0x10'u8, 0xF0'u8, # 5
        0xF0'u8, 0x80'u8, 0xF0'u8, 0x90'u8, 0xF0'u8, # 6
        0xF0'u8, 0x10'u8, 0x20'u8, 0x40'u8, 0x40'u8, # 7
        0xF0'u8, 0x90'u8, 0xF0'u8, 0x90'u8, 0xF0'u8, # 8
        0xF0'u8, 0x90'u8, 0xF0'u8, 0x10'u8, 0xF0'u8, # 9
        0xF0'u8, 0x90'u8, 0xF0'u8, 0x90'u8, 0x90'u8, # A
        0xE0'u8, 0x90'u8, 0xE0'u8, 0x90'u8, 0xE0'u8, # B
        0xF0'u8, 0x80'u8, 0x80'u8, 0x80'u8, 0xF0'u8, # C
        0xE0'u8, 0x90'u8, 0x90'u8, 0x90'u8, 0xE0'u8, # D
        0xF0'u8, 0x80'u8, 0xF0'u8, 0x80'u8, 0xF0'u8, # E
        0xF0'u8, 0x80'u8, 0xF0'u8, 0x80'u8, 0x80'u8, # F
    ]
    FontAddr = 0x00

import displays/abstract as display
import controls/abstract as controls

type
    Ram* = array[RamSize, uint8]
    Registers = array[16, uint8]
    Stack = array[16, uint16]
    Machine* = ref object
        running*: bool
        ram*: Ram
        # 16 general purpose registers: V0..VF where VF is used for flags
        registers*: Registers
        # 16bit i register, only lower 12bits used
        i*: uint16
        # program counter
        pc*: uint16
        # 16 16bit locations
        stack*: Stack
        # stack pointer
        sp*: uint8
        display*: ADisplay
        control*: AControl
        soundTimer: uint8
        delayTimer: uint8


proc loadFonts(machine: Machine, startAddr: int = FontAddr) =
    for i, b in Fonts:
        machine.ram[startAddr+i] = b


proc newMachine*(display: ADisplay, control: AControl): Machine =
    new(result)
    result.display = display
    result.control = control
    result.pc = EntryPoint
    loadFonts(result, FontAddr)
    randomize()


proc reset*(machine: Machine) =
    machine.running = false
    machine.ram.fill(0)
    machine.registers.fill(0)
    machine.i = 0
    machine.pc = EntryPoint
    machine.stack.fill(0)
    machine.sp = 0
    loadFonts(machine, FontAddr)

func `$`(machine: Machine): string =
    "Machine:\n  running: " & $machine.running

proc todo() =
    raise newException(Exception, "@TODO")


iterator lines(ram: Ram, size: int = 32): tuple[
    lineCount: int, address: uint16, line: string, allZeros: bool] =
    ## iterate over ram in chunks of size bytes
    var 
        line = ""
        allZeros = true
        lineCount = 1
    for i, x in ram:
        line.add(x.toHex)
        if x != 0:
            allZeros = false
        if i mod 2 == 1: line.add(' ')
        if i mod 16 == 15: line.add(' ')
        if i mod 32 == 31: 
            yield (lineCount, cast[uint16](i), line, allZeros)
            allZeros = true
            line = ""
            lineCount += 0

proc toStr(ram: Ram, indent=0): string =
    let indentStr = repeat(" ", indent)
    var lastLineWritten = 0
    for i, address, line, allZeros in ram.lines():
        if not allZeros:
            if (i - lastLineWritten) > 1:
                result.add("...\n")
            result.add(address.toHex() & ": " & line)
            result.add("\n" & indentStr)
            lastLineWritten = i
    if result == "":
        result = "..."
    else:
        result = "\n" & result

func toStr(registers: Registers): string =
    for register in registers:
        result.add(register.toHex)
        result.add(' ')

func toStr(stack: Stack, sp: uint8): string =
    for i, item in stack:
        if cast[uint8](i) == sp: 
            result.add('*')
        result.add(item.toHex)
        result.add(' ')

func toTableStr(xxx: seq[tuple[key: string, val: string]], indent=4): string =
    func fn(s: string, width: int): string =
        let diff = max(width - s.len, 0)
        result = s & repeat(" ", diff)

    var 
        header = ""
        line = ""
    for key, val in xxx.items():
        let width = max(key.len, val.len)
        header.add(fn(key, width) & " ")
        line.add(fn(val, width) & " ")
    result = header & "\n" & repeat(" ", indent) & line


proc display*(machine: Machine) =
    var header = @[
        ("pc", machine.pc.toHex), ("i", machine.i.toHex), 
    ]
    for i, register in machine.registers:
        let name = "v" & cast[uint8](i).toHex[1 .. ^1]
        add(header, (name, register.toHex))

    echo "Machine: running: $1\n       $2\nstack: $3\nram: $4" % [
        $machine.running, 
        toTableStr(header, indent=7),
        machine.stack.toStr(machine.sp),
        machine.ram.toStr(indent=0)]

proc loadRomData*(machine: Machine, data: seq[uint8], start: int = EntryPoint) =
    for i, x in data:
        machine.ram[start + i] = x

proc nextInstruction(machine: Machine): uint16 =
    if machine.pc >= RamSize: raise newException(Exception, "End of ram!")
    result = (machine.ram[machine.pc] shl 8) or (machine.ram[machine.pc+1])
    machine.pc += 2

proc address(ins: uint16): uint16 {.inline.} =
    (ins and 0x0FFF)

proc vx(ins: uint16): uint8 {.inline.} =
    cast[uint8]((ins and 0x0F00) shr 8)

proc vy(ins: uint16): uint8 {.inline.} =
    cast[uint8]((ins and 0x00F0) shr 4)

proc byte(ins: uint16): uint8 {.inline.} =
    cast[uint8](ins and 0x00FF)

proc nibble(ins: uint16): uint8 {.inline.} =
    cast[uint8](ins and 0x000F)

proc vx(machine: Machine, ins: uint16): uint8 {.inline.} =
    machine.registers[vx(ins)] 

proc vy(machine: Machine, ins: uint16): uint8 {.inline.} =
    machine.registers[vy(ins)] 

proc undefinedOperation() = discard

proc processInstruction(machine: Machine, instruction: uint16) =
    case (instruction and 0xF000)
    of 0x0000:
        case instruction:
        # CLS
        of 0x00E0: machine.display.clear()
        # RET
        of 0x00EE:
            assert machine.sp > 0
            machine.sp -= 1
            machine.pc = machine.stack[machine.sp]
        else: undefinedOperation()
    # JP nnn
    of 0x1000:
        machine.pc = address(instruction)
    # CALL nnn
    of 0x2000:
        assert machine.sp < 16
        machine.stack[machine.sp] = machine.pc
        machine.sp += 1
        machine.pc = address(instruction)
    # SE Vx, byte
    of 0x3000:
        if byte(instruction) == machine.vx(instruction):
            machine.pc += 2
    # SNE Vx, byte
    of 0x4000:
        if byte(instruction) != machine.vx(instruction):
            machine.pc += 2
    # SE Vx, vy
    of 0x5000:
        assert (instruction and 0x000F) == 0
        if machine.vy(instruction) == machine.vx(instruction):
            machine.pc += 2
    # LD Vx, byte
    of 0x6000: machine.registers[vx(instruction)] = instruction.byte()
    # ADD Vx, byte
    # Vx = Vx + kk
    of 0x7000: machine.registers[vx(instruction)] += instruction.byte()
    of 0x8000: 
        case (instruction and 0x000F)
        # LD Vs, Vy
        # Vx = Vy
        of 0x0000: machine.registers[vx(instruction)] = machine.vy(instruction)
        # OR Vx, Vy
        of 0x0001:
            machine.registers[vx(instruction)] = machine.vx(instruction) or machine.vy(instruction)
        # AND Vx, Vy
        of 0x0002:
            machine.registers[vx(instruction)] = machine.vx(instruction) and machine.vy(instruction)
        # XOR Vx, Vy
        of 0x0003:
            machine.registers[vx(instruction)] = machine.vx(instruction) xor machine.vy(instruction)
        # ADD Vx, Vy
        of 0x0004: 
            let r = machine.vx(instruction) + machine.vy(instruction)
            if r > 0xFF:
                machine.registers[0xF] = 1
            else:
                machine.registers[0xF] = 0
            machine.registers[vx(instruction)] = cast[uint8](r)
        # SUB Vx, Vy
        of 0x0005: 
            let 
                x = machine.vx(instruction)
                y = machine.vy(instruction)
            if x > y:
                machine.registers[0xF] = 1
            else:
                machine.registers[0xF] = 0
            machine.registers[vx(instruction)] = x - y
        # SHR Vx, Vy
        of 0x0006:
            let 
                x = machine.vx(instruction)
            if (x and 1) == 1:
                machine.registers[0xF] = 1
            else:
                machine.registers[0xF] = 0
            machine.registers[vx(instruction)] = x shr 2
        # SUBN Vx {, Vy}
        of 0x0007:
            let 
                x = machine.vx(instruction)
                y = machine.vy(instruction)
            if y > x:
                machine.registers[0xF] = 1
            else:
                machine.registers[0xF] = 0
            machine.registers[vx(instruction)] = y - x 
        # SHL Vx, Vy
        of 0x000E:
            let x = machine.vx(instruction)
            if (x and 0b1000_0000) == 0b1000_0000:
                machine.registers[0xF] = 1
            else:
                machine.registers[0xF] = 0
            machine.registers[vx(instruction)] = x shl 2
        else: undefinedOperation()
    # SNE Vx, Vy
    of 0x9000: 
        if machine.vx(instruction) != machine.vy(instruction):
            machine.pc += 2
    # LD I, nnn
    of 0xA000: 
        machine.i = address(instruction)
    # JP V0, nnn
    of 0xB000: 
        machine.pc = address(instruction) + machine.registers[0]
    # RND Vx, kk
    of 0xC000: 
        machine.registers[vx(instruction)] = cast[uint8](rand(255)) and byte(instruction)
    # DRW Vx, Vy, n
    of 0xD000: 
        let 
            n = machine.i + nibble(instruction)
            data = machine.ram[machine.i ..< n]
        machine.registers[0xF] = machine.display.draw(data, machine.vx(instruction), machine.vy(instruction))
    of 0xE000: 
        case (instruction and 0x00FF)
        # SKP Vx
        of 0x009E: 
            if machine.control.isPressed(machine.vx(instruction)):
                machine.pc += 2
        # SKNP Vx
        of 0x00A1: 
            if not machine.control.isPressed(machine.vx(instruction)):
                machine.pc += 2
        else: undefinedOperation()
    of 0xF000: 
        case (instruction and 0x00FF)
        # LD Vx, DT
        of 0x0007: 
            machine.registers[vx(instruction)] = machine.delayTimer
        # LD ST, Vx
        of 0x000A: 
            let k = machine.control.getKey()
            machine.registers[vx(instruction)] = k
        # LD DT, Vx
        of 0x0015: 
            machine.delayTimer = machine.vx(instruction)
        # LD ST, Vx
        of 0x0018: 
            machine.soundTimer = machine.vy(instruction)
        # ADD I, Vx
        of 0x001E: machine.i += machine.vx(instruction)
        # LD F, Vx
        of 0x0029: 
            machine.i = FontAddr + FontSize * machine.vx(instruction)
            assert machine.i <= RamSize
            assert machine.i <= (FontAddr + FontSize*0xF)
        # LD B, Vx
        of 0x0033: 
            let b = toBcd(machine.vx(instruction))
            machine.ram[machine.i] = b[0]
            machine.ram[machine.i+1] = b[1]
            machine.ram[machine.i+2] = b[2]
        # LD [I], Vx
        of 0x0055: 
            for i, v in machine.registers:
                machine.ram[machine.i + cast[uint8](i)] = v
        # LD Vx, [I]
        of 0x0065:
            for i in 0 ..< machine.registers.len:
                machine.registers[i] = machine.ram[machine.i + cast[uint8](i)]
        else: undefinedOperation()
    else: undefinedOperation()

proc step(machine: Machine) =
    let instruction = machine.nextInstruction()
    machine.processInstruction(instruction)
    # do timers

proc run(machine: Machine) =
    while machine.running:
        machine.step()

proc runRom*(machine: Machine, rom: seq[uint8]) =
    machine.reset()
    machine.loadRomData(rom)
    # machine.display()
    machine.run()


when isMainModule:
    # echo newMachine()
    var machine = newMachine()
    machine.reset()
    # machine.display()

