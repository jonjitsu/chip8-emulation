import strutils
import sugar
import sequtils

proc calculateColumnWidths(lines: seq[seq[string]]): seq[int] =
    result = newSeq[int](if lines.len > 0: lines[0].len else: 0)
    for line in lines:
        for i, item in line:
            result[i] = max(result[i], len(item))

proc atLeastWidth(s: string, width: int, padding: char = ' '): string =
    result=s
    if s.len < width:
        result=s & repeat(padding, width - s.len)

proc render*(lines: seq[seq[string]]): string =
    let widths = calculateColumnWidths(lines)

    lines.map(
        (columns: seq[string]) => 
            columns.map(
                (i: int, column: string) =>
                    column.atLeastWidth(widths[i])).join(" ")
    ).join("\n")