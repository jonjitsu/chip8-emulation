from sequtils import pairUp

proc word(msb: uint8, lsb: uint8): uint16 {.inline.} =
    result = msb
    result = (result shl 8) or lsb

# proc toUint16(data: seq[uint8]): seq[uint16] =
#     var
#         i = 0
#         x: uint16 = 0
#     while i < len(data):
#         x = x or data[i]
#         if (i mod 2) == 0:
#             x = x shl 8
#         else:
#             result.add(x)
#             x = 0
#         i += 1
#     if (i mod 2) == 1:
#         result.add(x)
proc toUint16*(data: seq[uint8]): seq[uint16] =
    for msb, lsb in data.pairUp():
        result.add(word(msb, lsb))

proc loadRomFromFile*(filepath: string): seq[uint8] =
    let fp = open(filepath, fmRead)
    defer: fp.close()
    cast[seq[uint8]](fp.readAll())


if isMainModule:
    doAssert word(0xA2, 0x1F) == 0xA21F
    doAssert @[0x10'u8].toUint16() == @[0x1000'u16]
    doAssert @[0x10'u8, 0x22'u8].toUint16() == @[0x1022'u16]
    doAssert @[0x10'u8, 0x22'u8, 0xF6'u8].toUint16() == @[0x1022'u16, 0xF600]