import abstract

type
    NoopDisplay* = ref object of ADisplay

method clear*(d: NoopDisplay) =
    discard
