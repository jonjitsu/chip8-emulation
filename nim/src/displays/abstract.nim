
type
    ADisplay* = ref object of RootObj

method clear*(d: ADisplay) {.base.} = 
    discard
    # raise newException(Exception, "NotImplementedError")

method draw*(d: ADisplay, data: openArray[uint8], x: uint8, y: uint8): uint8 {.base.} =
    0