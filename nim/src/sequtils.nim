import std/sequtils
export sequtils

proc map*[T, S](s: openArray[T], op: proc (i: int, x: T): S {.closure.}):
                                                            seq[S]{.inline.} =
  ## Returns a new sequence with the results of `op` proc applied to every
  ## item in the container `s`.
  ##
  ## Since the input is not modified you can use it to
  ## transform the type of the elements in the input container.
  ##
  ## See also:
  ## * `mapIt template<#mapIt.t,typed,untyped>`_
  ## * `apply proc<#apply,openArray[T],proc(T)_2>`_ for the in-place version
  ##
  runnableExamples:
    let
      a = @[1, 2, 3, 4]
      b = map(a, proc(i: int, x: int): (int, int) = (i, x))
    assert b == @[(0, 1), (1, 2), (2, 3), (3, 4)]

  newSeq(result, s.len)
  for i in 0 ..< s.len:
    result[i] = op(i, s[i])


iterator pairUp*[T](items: openArray[T], filler: T = default(T)): (T, T) =
  ## Iterates over a sequence of things yielding each pair.
  ## a sequence:
  ## [x_1, x_2, ..., x_n-1, x_n] 
  ## becomes
  ## [(x_1, x_2), ..., (x_n-1, x_n)]
  ## 
  ## If the sequence of things is odd then filler is used.
  runnableExamples:
    var pairs: seq[(int, int)] = @[]
    for pair in pairUp(@[1, 2, 3, 4]):
      pairs.add(pair)
    assert pairs == @[(1, 2), (3, 4)]

  runnableExamples:
    var pairs: seq[(int, int)] = @[]
    for pair in pairUp(@[1, 2, 3], 0):
      pairs.add(pair)
    assert pairs == @[(1, 2), (3, 0)], "pairs: " & $pairs

  runnableExamples:
    var pairs: seq[(char, char)] = @[]
    for pair in pairUp(@['1']):
      pairs.add(pair)
    assert pairs == @[('1', '\0')], "pairs: " & $pairs

  var
    i = 0
    first: T
  while i < len(items):
    if (i mod 2) == 0:
      first = items[i] 
    else:
      yield (first, items[i])
    i += 1

  if (i mod 2) == 1:
    yield (first, filler)

    
iterator chunk*[T](items: openArray[T], n: int, filler: T = default(T)): seq[T] =
  ## Iterates over a sequence of things yielding sequences of size n.
  ## a sequence:
  ## [x_1, x_2, ..., x_n-1, x_n] 
  ## becomes
  ## [[x_1, x_2, ..., x_m%n], ..., [x_m-n, ..., x_m-1, x_m]]
  ## 
  ## If the sequence of things is odd then filler is used.
  runnableExamples:
    block:
      var pairs: seq[seq[int]] = @[]
      for pair in chunk(@[1, 2, 3, 4], 2):
        pairs.add(pair)
      assert pairs == @[@[1, 2], @[3, 4]]
    block:
      var pairs: seq[seq[int]] = @[]
      for pair in chunk(@[1, 2, 3], 2, 0):
        pairs.add(pair)
      assert pairs == @[@[ 1, 2 ], @[ 3, 0 ]], "pairs: " & $pairs
    block:
      var pairs: seq[seq[char]] = @[]
      for pair in chunk(@['1'], 3):
        pairs.add(pair)
      assert pairs == @[@['1', '\0', '\0']], "pairs: " & $pairs

  var
    i = 0
    chunk = newSeq[T](n)
  while i < len(items):
    chunk[i mod n] = items[i]
    if (i mod n) == (n-1):
      yield chunk
    i += 1

  if (i mod n) != 0:
    for x in (i mod n) .. (n - 1):
      chunk[x] = filler
    yield chunk
    